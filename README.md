Deliver a scanned document
==========================

This software can read an incoming email, usually from your scanner, and re-send it.

Images are extracted from PDF, recompressed with [mozjpeg](https://github.com/mozilla/mozjpeg),
and added to a new email.

Postfix configuration
---
Simply add an alias (usually in `/etc/aliases`:
```
scan:|/usr/local/bin/deliver_scan.py
```

All emails sent to `scan+<user>@example.com` are re-sent to `<user>@example.com`.

If `recipient_delimiter` is set to another value than `+`, then the above example
should be modified accordingly.

Inline configuration
---
`MOZJPEG_TRAN` is the path to a `jpegtran` executable, with arguments.
`FROM_ADDR` is the `From` field for re-sent emails.
`DEFAULT_RECIPIENT` is the `To` field when there is no delimiter in the received email recipient.