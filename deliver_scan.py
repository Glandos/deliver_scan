#!/usr/bin/env python3

import shutil
import itertools
import tempfile
import email
import email.policy
import contextlib
import pathlib
import smtplib
import mimetypes
import datetime
import sys
import uuid

import sh


MOZJPEG_TRAN = ["/opt/mozjpeg/jpegtran", "-copy", "none", "-outfile"]

FROM_ADDR = "scan@example.com"
DEFAULT_RECIPIENT = "user@example.com"

SMTP_SERVER = "mail.example.com"

"""Bits & Bytes related humanization.
Extracted from MIT code at https://github.com/jmoiron/humanize/blob/master/humanize/filesize.py
"""

suffixes = {
    "decimal": ("kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"),
    "binary": ("KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB"),
    "gnu": "KMGTPEZY",
}


def naturalsize(value, binary=False, gnu=False, format="%.1f"):
    """Format a number of byteslike a human readable filesize (eg. 10 kB).  By
    default, decimal suffixes (kB, MB) are used.  Passing binary=true will use
    binary suffixes (KiB, MiB) are used and the base will be 2**10 instead of
    10**3.  If ``gnu`` is True, the binary argument is ignored and GNU-style
    (ls -sh style) prefixes are used (K, M) with the 2**10 definition.
    Non-gnu modes are compatible with jinja2's ``filesizeformat`` filter."""
    if gnu:
        suffix = suffixes["gnu"]
    elif binary:
        suffix = suffixes["binary"]
    else:
        suffix = suffixes["decimal"]

    base = 1024 if (gnu or binary) else 1000
    bytes = float(value)

    if bytes == 1 and not gnu:
        return "1 Byte"
    elif bytes < base and not gnu:
        return "%d Bytes" % bytes
    elif bytes < base and gnu:
        return "%dB" % bytes

    for i, s in enumerate(suffix):
        unit = base ** (i + 2)
        if bytes < unit and not gnu:
            return (format + " %s") % ((base * bytes / unit), s)
        elif bytes < unit and gnu:
            return (format + "%s") % ((base * bytes / unit), s)
    if gnu:
        return (format + "%s") % ((base * bytes / unit), s)
    return (format + " %s") % ((base * bytes / unit), s)


@contextlib.contextmanager
def temp_dir():
    d = tempfile.mkdtemp()
    yield pathlib.Path(d)
    shutil.rmtree(d)


def rework_pdf(pdf: pathlib.Path):
    now_suffix = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    output_pdf = pdf.with_name(f"{pdf.stem}_{now_suffix}{pdf.suffix}")
    sh.pdfimages("-j", pdf, pdf.parent / pdf.stem)
    xarged_mozjpeg = sh.xargs.bake(
        "--null", "--max-procs=3", "-I{}", *MOZJPEG_TRAN, "moz-{}", "{}"
    )
    files = list(pdf.parent.glob(f"{pdf.stem}-*.jpg"))
    xarged_mozjpeg(_in=sh.printf(r"%s\x00", *[f.name for f in files], _return_cmd=True), _cwd=pdf.parent)

    # Construct pdf
    moz_files = sorted(pdf.parent.glob(f"moz-{pdf.stem}-*.jpg"))
    sh.img2pdf("-o", output_pdf, *moz_files)

    # cleanup
    for f in itertools.chain(files, moz_files):
        f.unlink()

    return output_pdf


def send_scan(recipients, transformed_filenames):
    msg = email.message.EmailMessage()
    msg["Subject"] = "Recompression des documents"
    msg["From"] = FROM_ADDR
    msg["To"] = ", ".join(
        email.utils.formataddr((False, recipient)) for recipient in recipients
    )
    msg["Message-Id"] = f"<{uuid.uuid4()}@{FROM_ADDR.split('@', 1)[1]}>"
    msg["Date"] = email.utils.formatdate(localtime=True)
    # User-Agent to trick some antispam (e.g. free.fr)
    msg[
        "User-Agent"
    ] = "Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 Thunderbird/60.7.0"
    # This is normally added by set_content that we skip intentionnally
    msg["MIME-Version"] = "1.0"

    file_descriptions = []
    for old_filename, filename in transformed_filenames:
        if not filename.is_file():
            continue
        # Guess the content type based on the file's extension.  Encoding
        # will be ignored, although we should check for simple things like
        # gzip'd or compressed files.
        ctype, encoding = mimetypes.guess_type(str(filename))
        if ctype is None or encoding is not None:
            # No guess could be made, or the file is encoded (compressed), so
            # use a generic bag-of-bits type.
            ctype = "application/octet-stream"
        maintype, subtype = ctype.split("/", 1)
        msg.add_attachment(
            filename.read_bytes(),
            maintype=maintype,
            subtype=subtype,
            filename=filename.name,
        )
        old_size = old_filename.stat().st_size
        new_size = filename.stat().st_size
        file_descriptions.append(
            f"- {old_filename.name} ({naturalsize(old_size)}) ➡ {filename.name} ({naturalsize(new_size)}): {1-new_size/old_size:.1%}"
        )

    file_descriptions = "\n".join(file_descriptions)
    content = email.message.EmailMessage()
    content.set_content(
        f"""Voici la liste des documents numérisés:
{file_descriptions}"""
    )
    msg._payload.insert(0, content)

    msg.preamble = "This is a multi-part message in MIME format."

    with smtplib.SMTP(SMTP_SERVER) as smtp:
        smtp.starttls()
        smtp.send_message(msg)


def get_recipients(msg: email.message.EmailMessage):
    initial_to = email.utils.getaddresses(msg.get_all("To"))
    final_to = set()
    for name, recipient in initial_to:
        localpart, domain = recipient.split("@", 1)
        finals = localpart.split("+")[1:]
        if finals:
            final_to.update([f"{final}@{domain}" for final in finals])
        else:
            final_to.add(DEFAULT_RECIPIENT)

    return final_to


def get_informations(f, working_directory):
    msg: email.message.EmailMessage = email.message_from_file(
        f, policy=email.policy.default
    )

    filenames = []
    for part in msg.iter_attachments():
        if part.get_content_type() == "application/pdf":
            filename = working_directory / part.get_filename()
            with open(filename, "wb") as attachement:
                attachement.write(part.get_content())
            new_attachment = rework_pdf(filename)
            filenames.append((filename, new_attachment))

    return get_recipients(msg), filenames


if __name__ == "__main__":
    if len(sys.argv) >= 2 and pathlib.Path(sys.argv[1]).is_file():
        opener = lambda: open(sys.argv[1])
    else:
        opener = lambda: contextlib.closing(sys.stdin)

    with opener() as f, temp_dir() as working_directory:
        recipients, attachments = get_informations(f, working_directory)
        send_scan(recipients, attachments)
